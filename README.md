# Stiffness control of constrained robotic manipulator

Lagrangian and Hamiltonian dynamics of 2-link robotic manipulator. Manipulator end-effector is constrained within a slot. Stiffness & hybrid control. Matlab simulations using P.Corke's toolbox (fix